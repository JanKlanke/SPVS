function design = genDesign(vpno, seno, condIn, dome, param, subjectCode)
% 2021 by Jan Klanke
% 
% Input:  vpno        - id of the participant
%         seno        - number of the session of the participant
%         cond        - number of the condition/s to be presented
%         dome        - dominant eye
%         ms          - array with microsaccade data for replay
%         subjectCode - subject code (experiment name + vpno + seno)
% 
% Output: design      - array with design structure of the experiment

global visual scr keys setting %#ok<NUSED>

% strictly for code-testing: Emulation of propixx on low res screen: 
if setting.Pixx == 2; scr.refr = 1440; scr.fd = 1/scr.refr; end

% randomize random
rand('state',sum(100*clock));

% function for rounding away from zero
paren = @(x, varargin) x(varargin{:});
ceilfix = @(x)ceil(abs(x)) .* sign(x);

% this subject's main sequence parameters
V0 = 450;Am= 1;A0 = 7.9;durPerDeg = 2.7;durInt = 23;

% participant parameters
design.vpno = vpno;
design.seno = str2double(seno);
design.dome = dome;

% condition parameter
design.fixPres = [1 1 1 1 1];                   %  0 = no fix, 1 = fix present    
design.stiPres = [0 1 1 2 2];                   %  0 = no stim, 1 = stim present    
design.appShif = [0 1 0 2 0];                   %  0 = no apperture shift, 1 = apperture shift                  
design.movDirs = [-1 1];                        % -1 = movement direction from right to left; 1= movement direction from left to right
design.sacCons = [0 1];                         %  0 = no saccade instruction, 1 = saccade instruction 1, 2 = saccade instruction 2, 
design.repJInst= [0];                           %  0 = no replay of saccade instruction, 1 = replay of saccade instruction
design.instDist= [0.5 1.0 1.5];

% smooth pursuit target parameter
design.velosDS = repmat([3 6 9],1,2);           % velocities [deg/s];
design.velosStr= repmat({'low' 'medium' 'high'},1,2);
velIdx = randperm(length(design.velosDS));
mixVelosDS = design.velosDS(velIdx);
mixVelosStr= design.velosStr(velIdx);

% no. of blocks and trials per condition
if setting.train == 0
    if length(condIn) < 3; condIn = condIn(end:-1:1); condIn= sort(condIn(mod(0:3-1,numel(condIn))+1)); end
    design.condRat =   [repmat(condIn(1),1,2),repelem(condIn(2:3),1,3)]; % [repmat(condIn(1:2),1,1),repmat(condIn(3),1,3)];                %  1 = no stim, 2 = simulated catch-up saccade, 3 = active simulated saccade
    design.nTrialsPerCellInBlock = 1; % 9;                                                %  number of trials per cell in block  
    trainFlag = 0;
else
    % practice:
    mixVelosDS  = [3 6 9];          % [3 6 9]
    mixVelosStr = {'low' 'medium' 'high'};
    design.nTrialsPerCellInBlock = 3;     % number of trials per cell in block
    
    % settings for stim. vis. practice
    if condIn == 6
        design.condRat = 2;
        mixVelosStr = repmat({'no'},1,3);
        trainFlag = 1;
        
    % settings for pursuit practice
    elseif condIn == 7                       
        design.condRat = 1;
        trainFlag = 2;
    
    % settings for exp. practice.
    elseif condIn == 8
        design.condRat = [2 3];
        trainFlag = 3;
    end
end
design.nBlocks = length(mixVelosDS);  
if setting.movie; design.condRat = 1;end

% setting for fixation (before cue)
design.timFixD = 0.500;                         % minimum fixation duration before stim onset [s]
design.timFixJ = 0.000;                         % additional fixation duration jitter before stim onset [s]

% setting for saccade  (before cue)
design.timSacDE = 0.700;                        % minimum duration before EARLY saccade init onset [s]
design.timSacDL = 1.150;                        % minimum duration before LATE  saccade init onset [s]
design.timSacJ  = 0.300;                        % additional duration jitter saccade init stim onset [s]
design.timFlaD  = 0.050;                        

% timing settings for stimulus
design.timAfKe = 0.200;                         % recording time after keypress  [s]
design.timMaSa = 1.000;                         % time to make a saccade (most likely)  [s]
design.timStiD = design.timMaSa;                % stimulus stream duration    [s]
design.iti     = 0.000;                         % inter-trial interval [s]

% fixation dot parameters 
design.fixSiz  = 0.1;      % radius of the fixation dot (inner part) [dva]
design.gosSiz  = 0.3;      % radius of the fixation dot (outer part) [dva]                    
design.tarSiz  = 0.22;     % size of the pursuit target in diameter [dva]

% stimulus parameters (artificial MS aperture shift)
design.numStim = 2;                                                        % number of stimuli
design.stiAngl = -pi:(2*pi/360):pi-(2*pi/360);                             % vector with possible stimulus orientations (0, 359 deg)
design.rawPhaVelDS= V0 * (1 - exp(-Am / A0));                              % default velocity of the phase shift velocity [dva/s]
design.appMoveEndpoint = [cos(design.stiAngl); sin(design.stiAngl)];       % vector with the X and Y coordinates of the endpoints of the apperture movements. The Y coordinate is flipped: sin() * -1! 
design.appMoveIdx      = repmat([359:360,1:3, 179:183],1,100);             % vector with indeces all of the possible apperture movements. Indceses are normally distributed

% scale parameters
design.scale.handN  = 1;                   % number of scale hands to be used 
design.scale.itemN  = 7;                   % number of items on the likert scale 
design.scale.length = 15;                  % scale length radius [dva]
design.scale.width  = .15;                 % scale width radius  [dva]
design.scale = getScalePars(design.scale); % generate scale based on parameters

% pre drawing of trialcount
c = 0; 
totNT = design.nBlocks*design.nTrialsPerCellInBlock*length(design.condRat)*length(design.movDirs)*length(design.sacCons)*length(design.repJInst)*length(design.instDist);

% loop through trials
for b = 1:design.nBlocks
    t = 0;
    velo = mixVelosDS(b);
    for cond = design.condRat 
        for itri = 1:design.nTrialsPerCellInBlock 
            for samo = design.sacCons
                for ddst = design.instDist
                    for rejn = design.repJInst
                        for modi = design.movDirs 

                            t = t + 1; c = c + 1;
                            fprintf(1,'\n preparing trial %i ...',t);
                            trial(t).trialNum = t; %#ok<*AGROW>

                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            %      Saccade parameter in diff. conditions      %
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            % Deault ms values (no-stimulus condition)
                            trial(t).sac.idx = NaN;
                            trial(t).sac.amp = 0;
                            trial(t).sac.vpk = 0;
                            trial(t).sac.dur = 0;
                            trial(t).sac.ang = NaN;
                            trial(t).sac.req = 0;

                            % prep stuff for replay
                            if all(seno == '01')
                                trial(t).shape = NaN;
                                trial(t).scale = NaN;
                                trial(t).A0 = A0;
                                trial(t).V0 = V0;
                            else
                                tIdx = find([param.tarvel] == velo);
                                trial(t).shape = param(tIdx).shape;
                                trial(t).scale = param(tIdx).scale;
                                trial(t).A0 = param(tIdx).A0;
                                trial(t).V0 = param(tIdx).V0;
                            end

                            % modifications if its the first session and the active and the
                            % repaly conditions are based on the data of a 'simulated' data
                            if  design.appShif(cond) == 1

                                % REPlay Index: In the simulated microsaccade condition determined by pregenerated saccade szenarios.
                                repi = ceilfix(design.appMoveIdx(ceilfix(length(design.appMoveIdx) * rand))); 
                                trial(t).sac.idx = repi;

                                % Expected saccade velocities based on amplitudebars
                                % (using equation and parameters used in Collewijn,
                                % 1988). Angle of saccade to be replayed is based on
                                % on the endpoints of the saccade that is replayed.
                                trial(t).sac.amp = Am;                                          % in dva
                                trial(t).sac.vpk = V0 * (1 - exp(-trial(t).sac.amp / A0));      % in dva/s (the actual peak velocity of the aperture motion is slightkly higher; around 35.5 dva/s
                                trial(t).sac.dur = durPerDeg * trial(t).sac.amp + durInt;       % should be around 25 ms according to Martinez-Conde et al. 2004 [ms]
                                trial(t).sac.ang = atan2(design.appMoveEndpoint(2, repi), design.appMoveEndpoint(1, repi)); % saccade orientation determined by predefined stimulus orientations [rad]                       

                            elseif design.appShif(cond) == 2

                                % REPlay Index: In the simulated microsaccade condition determined by pregenerated saccade szenarios.
                                repi = ceilfix(design.appMoveIdx(ceilfix(length(design.appMoveIdx) * rand))); 
                                trial(t).sac.idx = repi;

                                % Expected saccade velocities based on amplitudebars
                                % (using equation and parameters used in Collewijn,
                                % 1988). Angle of saccade to be replayed is based on
                                % on the endpoints of the saccade that is replayed.
                                trial(t).sac.amp = gamrnd(trial(t).shape, trial(t).scale);                        % in dva
                                trial(t).sac.vpk = trial(t).V0 * (1 - exp(-trial(t).sac.amp / trial(t).A0));      % in dva/s (the actual peak velocity of the aperture motion is slightkly higher; around 35.5 dva/s
                                trial(t).sac.dur = durPerDeg * trial(t).sac.amp + durInt;       % should be around 25 ms according to Martinez-Conde et al. 2004 [ms]
                                trial(t).sac.ang = atan2(design.appMoveEndpoint(2, repi), design.appMoveEndpoint(1, repi)); % saccade orientation determined by predefined stimulus orientations [rad]                       
                            end

                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            %            Temporal trial settings              %
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            % duration before stimulus onset [frames]
                            trial(t).fixNFr = round((design.timFixD + design.timFixJ * rand) / scr.fd);
                            trial(t).fixDur = trial(t).fixNFr * scr.fd;

                            % duration before stimulus onset [frames]
                            swti = rand < 0.5;                    
                            if ~swti      % early switch
                                trial(t).sacNFr = round((design.timSacDE + design.timSacJ * rand) / scr.fd);
                            else          % late  switch
                                trial(t).sacNFr = round((design.timSacDL + design.timSacJ * rand) / scr.fd); 
                            end
                            trial(t).sacDur = trial(t).sacNFr * scr.fd;

                            % duration after stimulus onset [frames]
                            trial(t).stiNFr = round(design.timStiD / scr.fd);
                            trial(t).stiDur = trial(t).stiNFr * scr.fd;
                            
                            % flash duration [frames]
                            trial(t).flaNFr = round(design.timFlaD / scr.fd);
                            trial(t).flaDur = trial(t).flaNFr * scr.fd;

                            % duration of stimulus contrast ram [frames]
                            trial(t).ramNFr = round((design.timStiD / 5) / scr.fd);
                            trial(t).ramDur = trial(t).ramNFr * scr.fd;

                            % timing parameters of apperture shift [frames]
                            % (saccade duration is a good approximation of the duration of
                            % the apperture shift)
                            trial(t).shiNFr = round((trial(t).sac.dur / 1000) / scr.fd);       
                            trial(t).shiDur = trial(t).shiNFr * scr.fd;

                            % calculate total stimulus duration [frames]
                            trial(t).totNFr = trial(t).fixNFr + trial(t).stiNFr;

                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            % Generate flag streams for stimulus presentation %
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
                            % time of the fixation interval           
                            fixBeg = 1;
                            fixEnd = fixBeg + trial(t).fixNFr - 1;      

                            % time that the stimulus is present
                            stiBeg = trial(t).fixNFr + 1;
                            stiEnd = stiBeg + trial(t).stiNFr - 1;

                            % time that the saccade target jumps (i.e.,
                            % participant is informed about the target
                            % location of their eye movement--also go
                            % signal eye movement initiation)
                            sacIni = trial(t).sacNFr;
                            flaDur = trial(t).flaNFr;

                            % time that the stimulus is at full contrast
                            ctrBeg = stiBeg + trial(t).ramNFr;                              
                            ctrEnd = stiEnd - trial(t).ramNFr; 

                            % begin and end of the time window in which the apperture MAY move
                            shiBeg = ctrBeg + round(trial(t).shiNFr / 2);
                            shiEnd = ctrEnd - round(trial(t).shiNFr / 2);

                            % time parameter of apperture movement
                            appBeg = NaN;appEnd = NaN;appTop = NaN;
                            if design.appShif(cond) >= 1
                                % begin and end of the ACTUAL movement; moment at which 
                                % the apperture velocity is highest
                                appTop = shiBeg + round(rand * (shiEnd - shiBeg));   
                                appBeg = appTop - round(trial(t).shiNFr / 2);
                                appEnd = appBeg + trial(t).shiNFr - 1;
                            end

                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            %              spatial information                %
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            % fixation positions
                            fdist = design.timFixD * velo;                                  % distance travelled by the smooth pursuit target during fixation [dva]
                            sdist = design.timStiD * velo;                                  % distance travelled by the smooth pursuit target during stim. presentatgion [dva]
                            tdist = fdist + sdist;                                          % total distance travelled by the smooth pursuit target [dva]

                            % fixx = -modi*(tdist/2) + modi*fdist + (-.5 + rand);             
                            fixx = -modi*(sdist/2) + (-.5 + rand);                          % eccentricity of fixation x (relative to screen center) - can be pos or neg [dva]
                            fixy = 0;                                                       % eccentricity of fixation y (relative to screen center) - can be pos or neg [dva]

                            % raw smooth pursuit target movement vector
                            runDur = design.timFixD + design.timStiD;                       
                            velVec = repmat((1/runDur)*(tdist*scr.fd),1, trial(t).totNFr);  % advance in pixel per frame [dva/fra]
                            posVec = cumsum(velVec);                                        % advance in position per frame [pix/fra]

                            % normalize position vector to position of stim interval
                            normPosVec = posVec - posVec(fixEnd);

                            % create n-dots for potential saccade targets 
                            minDots = 4 + (samo == 0);
                            maxDots = 8 + (samo == 0);
                            dotN  = randsample([minDots-2:maxDots-2],1);
                            dotX  = normrnd(0,1.0,1,dotN);
                            dotY  = normrnd(0,0.4,1,dotN);

                            % create params for actual saccade target
                            rangl = randsample([0:5,175:185,355:359]*(pi/180), 1);
                            dotX = [dotX, 0, cos(rangl) * ddst];
                            dotY = [dotY, 0, sin(rangl) * ddst];
                            dotT1 = dotN + 1;
                            dotT2 = dotN + 2;
                            dotN  = dotT2;

                            % correct overlap
                            n_dot = 1; to_n_dot = 1; reset = 0;
                             while n_dot < dotN
                                while to_n_dot < n_dot
                                    if ( ( dotX(n_dot)-dotX(to_n_dot) )^2 + ( dotY(n_dot)-dotY(to_n_dot) )^2 )^.5 < design.tarSiz
                                        % fprintf('%i  %i\n', n_dot, to_n_dot)
                                        if to_n_dot <= ( dotN - 2)
                                            dotX(to_n_dot) = normrnd(0,1.0,1);
                                            dotY(to_n_dot) = normrnd(0,0.4,1);
                                            to_n_dot = 1;
                                            continue;
                                        elseif to_n_dot == ( dotN - 1)
                                            dotX(1:to_n_dot) = normrnd(0,1.0,1,to_n_dot);
                                            dotY(1:to_n_dot) = normrnd(0,0.4,1,to_n_dot);
                                            reset = 1;
                                            continue;
                                        elseif to_n_dot == dotN
                                            dotX(to_n_dot) = cos(rangl) * ddst;
                                            dotY(to_n_dot) = sin(rangl) * ddst;
                                            to_n_dot = 1;
                                            continue;
                                        end
                                    end
                                    to_n_dot = to_n_dot + 1;
                                end
                                if to_n_dot == n_dot
                                    to_n_dot = 1;
                                    n_dot = n_dot + 1;
                                end
                                if reset == 1
                                    to_n_dot = 1;
                                    n_dot = 1;
                                    reset = 0;
                                end
                            end

                            % extract directions
                            if modi < 0; ddirH = 1 ; else ddirH = -1; end
                            ddirV  =  sign(dotY(dotT2) - dotY(dotT1)); 

                            % create raw target movement position vectors (dva)
                            taxxV = fixx + dotX + (modi * normPosVec)'; 
                            taxyV = fixy + dotY + zeros(1, trial(t).totNFr)';          

                            % fixation
                            trial(t).fixa.vis = zeros(1,trial(t).totNFr);
                            trial(t).fixa.vis(fixBeg:fixEnd) = 1;
                            fixxrselect = paren(fixx + dotX, dotT1);
                            fixyrselect = paren(fixy + dotY, dotT1);
                            trial(t).fixa.loc = visual.scrCenter + round(visual.ppd * [fixxrselect fixyrselect fixxrselect fixyrselect]);
                            trial(t).fixa.sin = round(design.fixSiz * visual.ppd);         % radius of the inner segment of the fix. point in [pix] 
                            trial(t).fixa.sou = round(design.gosSiz * visual.ppd);         % radius of the outer segment of the fix. point in [pix]
                            trial(t).fixa.col = visual.black;

                            % target
                            trial(t).targ.vis = zeros(1,trial(t).totNFr);
                            trial(t).targ.vis(fixBeg:stiEnd) = 1;
                            trial(t).targ.locX = visual.scrCenter(1) + round(visual.ppd * taxxV);
                            trial(t).targ.locY = visual.scrCenter(2) + round(visual.ppd * taxyV);
                            trial(t).targ.loc = zeros(trial(t).totNFr,4);
                            trial(t).targ.siz = round(design.tarSiz * visual.ppd);  % diameter of the pursuit target [pix] 
                            trial(t).targ.col = repmat(visual.black,3,dotN, trial(t).totNFr);

                            tcol = 0.8; % visual.white
                            if samo == 1
%                                 trial(t).fixa.col = tcol;
%                                 trial(t).targ.col(:,dotT2, sacIni:sacIni+flaDur) = repmat(tcol,3,1, flaDur+1);
%                                 rdegr = round(atan2((dotY(dotT2)-dotY(dotT1)), (dotX(dotT2)-dotX(dotT1)))*(180/pi)) * -1;
%                                 
%                             elseif samo == 2
                                trial(t).fixa.col = tcol;
                                trial(t).targ.col(:,dotT1, fixEnd:sacIni-1) = repmat(tcol,3,1, sacIni-fixEnd);
                                trial(t).targ.col(:,dotT2, sacIni:sacIni+flaDur) = repmat(tcol,3,1, flaDur+1);
                                rdegr = round(atan2((dotY(dotT2)-dotY(dotT1)), (dotX(dotT2)-dotX(dotT1)))*(180/pi)) * -1;
                                                                 
                                % replay jump instruction
                                if rejn == 1
                                    dur    =  durPerDeg * ddst + durInt;
                                    baseGlide = repmat(normcdf(stiBeg:stiEnd, sacIni+dur*.5, dur/3)',1,size(trial(t).targ.locX,2));
                                    glideX = modi * ddirH * ddst * baseGlide * cos((2*pi-pi/180)-rangl) * scr.ppd;
                                    glideY =        ddirV * ddst * baseGlide * sin((2*pi-pi/180)-rangl) * scr.ppd;
                                    trial(t).targ.locX(stiBeg:stiEnd,:) = trial(t).targ.locX(stiBeg:stiEnd,:) + glideX;
                                    trial(t).targ.locY(stiBeg:stiEnd,:) = trial(t).targ.locY(stiBeg:stiEnd,:) + glideY;
                                end
                            else
                                rejn  = NaN;
                                sacIni= NaN;
                                ddst  = NaN;
                                rdegr = NaN;
                                ddirV = NaN;
                            end

                            % boundry
                            trial(t).bound.pos = zeros(trial(t).totNFr,4);
                            trial(t).bound.pos(fixBeg:fixEnd,:) = repmat(trial(t).fixa.loc,trial(t).fixNFr,1);   
                            
                            if samo > 0
                                trial(t).bound.pos(stiBeg:(sacIni+flaDur),:)  = [trial(t).targ.locX(stiBeg:(sacIni+flaDur), dotT1), trial(t).targ.locY(stiBeg:(sacIni+flaDur),  dotT1), trial(t).targ.locX(stiBeg:(sacIni+flaDur), dotT1), trial(t).targ.locY(stiBeg:(sacIni+flaDur), dotT1)];
                                trial(t).bound.pos((sacIni+flaDur):end,:)     = [trial(t).targ.locX((sacIni+flaDur):end,    dotT2), trial(t).targ.locY((sacIni+flaDur):end,     dotT2), trial(t).targ.locX((sacIni+flaDur):end,    dotT2), trial(t).targ.locY((sacIni+flaDur):end,    dotT2)];
                            else
                                trial(t).bound.pos(stiBeg:end,:) = [trial(t).targ.locX(stiBeg:end, dotT1), trial(t).targ.locY(stiBeg:end, dotT1), trial(t).targ.locX(stiBeg:end, dotT1), trial(t).targ.locY(stiBeg:end, dotT1)];
                            end
                            trial(t).bound.rad = zeros(1,trial(t).totNFr);
                            trial(t).bound.rad(fixBeg:fixEnd) =           visual.fixCkRad;
                            trial(t).bound.rad(stiBeg:stiEnd) = round(9 * visual.ppd); 
                            
                            % determine stimulus parameters
                            % spatial stimulus settings (standard)
                            trial(t).stims.col  = repmat(visual.black,design.numStim,1);
                            trial(t).stims.pars = getStimulusPars(design.numStim);
                            if design.stiPres(cond) == 0; trial(t).stims.pars.ori = NaN(1,design.numStim);
                            else
                                trial(t).stims.pars.ori = repmat(paren([0 180], round(1+rand)),design.numStim,1);
                                trial(t).stims.pars.ori(2) = 180 - trial(t).stims.pars.ori(2);    
                            end

                            % change all the settings for simple no-eyetracking stim.
                            % vis. practice trials
                            if trainFlag == 1
                                trial(t).stims.pars.amp = 0.75;                    % increase stim. contrast
                                trial(t).targ.vis = zeros(1,trial(t).totNFr);      % make target invisible again
                                trial(t).fixa.vis = ones(1,trial(t).totNFr);       % make fixation dot visible for entire trial
                                trial(t).fixa.loc = visual.scrCenter;              % place fixation at screen center
                                trial(t).bound.pos= repmat(trial(t).targ.loc,1, trial(t).totNFr); % align tracking boundary with screen center
                            end

                            % determine stimulus parameters
                            % spatial stimulus settings (standard)
                            trial(t).stims.locX = repmat(scr.centerX,design.numStim,1); 
                            trial(t).stims.locY = repmat(scr.centerY,design.numStim,1) + (trial(t).stims.pars.higp'.* [-1.5 1.5]');

                            % define change in stimulus position across frames
                            appmdX=NaN;appmdY=NaN;appdur=NaN; appvpk= NaN;               % defaults
                            trial(t).posVec = zeros(design.numStim * 2,trial(t).totNFr); 

                            % Stimulus position changes in the simulated MS and MS replay conditions:
                            if design.appShif(cond) > 0
                                if design.appShif(cond) >= 1
                                    % Calculate and correct replay index so that it the
                                    % aperture moves in the OPPOSITE direction of the
                                    % saccade it is based on. 
                                    repi = 180 + repi;
                                    repi(repi > 360) = repi(repi > 360) - 360;

                                    % Calculate changes in position vector (0:1).
                                    trial(t).posVec(:,shiBeg:shiEnd) = repmat(normcdf(shiBeg:shiEnd, appTop, trial(t).sac.dur/3), 2*design.numStim, 1);
                                    trial(t).posVec(:,shiEnd + 1:stiEnd) = repmat(trial(t).posVec(:,shiEnd),1,length(shiEnd + 1:stiEnd));

                                    % Scale changes by actual saccade amplitude and
                                    % rotate by angle. Also translates degrees to pixels. 
                                    trial(t).posVec = (repmat(trial(t).sac.amp .* design.appMoveEndpoint(:,repi),design.numStim,1) .* trial(t).posVec) * visual.ppd;
                                end   

                                % Flip y dimension to account for the different
                                % coordinate system of the screen
                                trial(t).posVec(2,:) = trial(t).posVec(2,:) * -1;

                                % Compute some chararteristics of the apperture
                                % movement for the output
                                appmdX = trial(t).posVec(1,appEnd) * visual.dpp;       % store final x coordinate for later storage [dva]
                                appmdY = trial(t).posVec(2,appEnd) * visual.dpp;       % store final y coordinate for later storage [dva]           
                                appdur = length(appBeg:appEnd);                        % calculate the duration of the apperture shift [ms]   

                                % Get peak velocoty of aperture motion in dva/s.
                                appvel = diff(trial(t).posVec, 1,2);
                                appvpk = max(sqrt(appvel(1,:).^2 + appvel(2,:).^2) * scr.dpp * scr.refr);
                            end

                            % define modulation of top velocity across frames
                            % (here, we are keeping stimulus velocity constant)
                            velVec = ones(1,length(stiBeg:stiEnd));
                            trial(t).stims.tmpfrq = 0; 

                            % calculate the evolution of the velocity of the phase shift
                            if design.stiPres(cond) > 0
                                phaVelDS = design.rawPhaVelDS + velo;  % add target velocity to phase shift [dva/2]
                            else
                                phaVelDS= NaN; 
                            end
                            trial(t).stims.evovel = repmat(velVec*phaVelDS,design.numStim,1) + repmat(trial(t).stims.tmpfrq ./ trial(t).stims.pars.frq',1,length(velVec)); % desired stimulus velocity [dva per sec]

                            % define phase change per frame for entire profile
                            phaFra = scr.fd*trial(t).stims.evovel.*repmat(trial(t).stims.pars.frq'*360,1,length(velVec));  % phase change per frame [deg  frame]

                            % define stimulus visibility and velocity
                            trial(t).stims.vis = zeros(1,trial(t).totNFr);
                            trial(t).stims.pha = zeros(design.numStim,trial(t).totNFr);
                            trial(t).stims.vis(:,stiBeg:stiEnd) = design.stiPres(cond) > 0;
                            trial(t).stims.pha(:,stiBeg:stiEnd) = cumsum(phaFra,2);         % in [deg / frame]

                            % add random phase to each stimulus
                            trial(t).stims.pha = trial(t).stims.pha + repmat(360* rand(design.numStim,1),1,trial(t).totNFr); % in [dpc / frame]

                            % recalculate phase shift in pixels per frame
                            trial(t).stims.pha = trial(t).stims.pha.*(scr.ppd./(trial(t).stims.pars.frq'*360));    % in [pix / frame]

                            % make sure the sine grating is only shifted by its size in the
                            % horizontal plane (i.e. the extent of the screen plus 1 phase
                            % [in pix].
                            f = repmat(2.*scr.resX,design.numStim,1) + 1./trial(t).stims.pars.frqp';
                            trial(t).stims.pha = mod(trial(t).stims.pha,f);               % in [pix / frame]

                            % define modulation of contrast across time
                            ampVec = ones(1,length(stiBeg:stiEnd));
                            ramVec = normcdf(1: trial(t).ramNFr, trial(t).ramNFr/2, trial(t).ramNFr/6);
                            ampVec(1: trial(t).ramNFr) = ramVec;
                            ampVec(end:-1:(end- trial(t).ramNFr+1)) = ramVec;
                            trial(t).stims.evoamp = repmat(trial(t).stims.pars.amp',1,length(ampVec)) .* repmat(ampVec,design.numStim,1);
                            trial(t).stims.amp = zeros(design.numStim,trial(t).totNFr);
                            trial(t).stims.amp(:,stiBeg:stiEnd) = trial(t).stims.evoamp;

                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            %         response & feedback information         %
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            trial(t).scale = design.scale;
                            sRand = ceil(size(trial(t).scale.lvl(3).a.labels,2)*rand);                               % random onset position of the scale
                            trial(t).scale.line.pos = trial(t).scale.line.posP + visual.scrCenter(1:2)';              % center scale on fixation dot
                            trial(t).scale.hand.posvecP = trial(t).scale.hand.posvecP + visual.scrCenter(1:2)';       % center response promt on fixation dot.
                            for i= 1:length(trial(t).scale.lvl)                                
                                trial(t).scale.lvl(i).q.posP = trial(t).scale.lvl(i).q.posP + visual.scrCenter(1:2);  % center question text on fixation dot.
                                trial(t).scale.lvl(i).a.posP = trial(t).scale.lvl(i).a.posP + visual.scrCenter(1:2);  % center response options on fixation dot.
                            end

                            % Block name messages 
                            if setting.train
                                if     trainFlag == 1; trial(t).message = sprintf('This is a training block for stimulus detection practice: %s speed condition', mixVelosStr{b});
                                elseif trainFlag == 2; trial(t).message = sprintf('This is a training block for smooth pursuit practice: %s speed condition', mixVelosStr{b});
                                elseif trainFlag == 3; trial(t).message = sprintf('This is a training block like the real experiment: %s speed condition', mixVelosStr{b});
                                end
                            else trial(t).message = sprintf('%s speed condition.', mixVelosStr{b});
                            end

                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            % define critical events during stimulus presentation %
                            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                            trial(t).events = zeros(1,trial(t).totNFr);
                            trial(t).events(fixBeg)                 = 1;     
                            trial(t).events(stiBeg)                 = 2;
                            trial(t).events(ctrBeg)                 = 3;
                            trial(t).events(sacIni(~isnan(sacIni))) = 4;
                            trial(t).events(appBeg(~isnan(appBeg))) = 5;
                            trial(t).events(appTop(~isnan(appTop))) = 6;
                            trial(t).events(ctrEnd)                 = 7;
                            trial(t).events(stiEnd)                 = 8;

                            % time requirements for responses
                            trial(t).maxSac = design.timMaSa;
                            trial(t).aftKey = design.timAfKe;

                            % store trial features
                            trial(t).feedback = setting.train;                  % 1 = feedback, 0 = no feedback
                            trial(t).expcon   = cond;                           % 1 = no stimulus, simulated microsaccade condition, active microsacccade condition

                            % store stimulus features
                            trial(t).fixpox = fixx;                             % x coordinate of position of fixation [dva rel. to midpoint]
                            trial(t).fixpoy = fixy * -1;                        % y coordinate of position of fixation FLIPPED [dva rel. to midpoint]
                            trial(t).movdir = modi;                             % motion direction of the smooth pursuit target -1= right:left, 1= left:right                    
                            trial(t).movvel = velo;                             % motion velocity of the smooth pursiut target [dva/s]
                            trial(t).phavel = phaVelDS;                         % velocity of the phase shift [dva/s]
                            trial(t).stioriUP = 180 - trial(t).stims.pars.ori(1); % stimulus orientation FLIPPED BACK to saccade coordinates: 0 = vertical, phase shift dir. right, 180 = vertical, phase shift dir.left
                            trial(t).stioriDW = 180 - trial(t).stims.pars.ori(2); % stimulus orientation FLIPPED BACK to saccade coordinates: 0 = vertical, phase shift dir. right, 180 = vertical, phase shift dir.left
                            trial(t).numDot = dotN; 
                            trial(t).sacMod = samo;
                            trial(t).sacDst = round(ddst,2);
                            trial(t).sacAng = rdegr;
                            trial(t).swiTim = swti;
                            trial(t).repJIn = rejn;
                            trial(t).appbeg = (appBeg - fixEnd) * scr.fd * 1000;% time point at which apperture shift starts [ms]
                            trial(t).apptop = (appTop - fixEnd) * scr.fd * 1000;% time point at which apperture shift reaches max velocity [ms]
                            trial(t).appdur = appdur * scr.fd * 1000;           % duration of the displayed microsaccade [ms]
                            trial(t).appmdX = appmdX;                           % x coordinate of position shift of the apperture [dva]
                            trial(t).appmdY = appmdY * -1;                      % y coordinate of position shift of the apperture FLIPPED BACK to saccade coordinates [dva]
                            trial(t).appori = round(rad2deg(atan2(trial(t).appmdY,trial(t).appmdX))); % appeture shift orientation FLIPPED BACK to saccade coordinates [deg]   
                            trial(t).appvpk = appvpk;                           % peak velocity of the aperture movement [dva/s]
                            trial(t).appamp = sqrt(appmdX^2 + appmdY^2);        % amplitude of the position shift of the apperture (here the difference in coordinate systems does not matter) [dva]

                            % store some scale features
                            trial(t).sOnPos = sRand;                            % onset position of scale hand [frame idx]

                            %%%%%%%%%%%%%%%%%%%%%%%
                            % draw loading screen %
                            %%%%%%%%%%%%%%%%%%%%%%%
                            drawLoadingScreen(visual.scrCenter, 5, c, totNT);
                        end
                    end
                end
            end
        end
    end
    ri = round(rand);
    r1 = randperm(t);                                           % randomise the trials per block
    r2 = reshape([r1(mod(r1,2)==ri); r1(mod(r1,2)~=ri)],[],1)'; % sort randomized trials into even and odd and put them together
    if ~setting.train
        design.b(:,b).train = [];
        design.b(:,b).trial = trial(r2);     % randomise in which order the condtions are presented (in experiment)
    elseif setting.train
        design.b(:,b).train = trial(r2);     % randomise in which order the condtions are presented (in training)
        design.b(:,b).trial = [];
    end
end

design.nBlocks = b;                             % this number of blocks is shared by all qu
design.blockOrder = randperm(b);                % org: design.blockOrder = 1:b;

design.nTrialsPB = t;                           % number of trials per Block

% strictly for testing: Emulation of propixx on low res screen: 
if setting.Pixx == 2; scr.refr = 60; scr.fd = 1/scr.refr; setting.Pixx = 0; end
 
save(sprintf('%s.mat',subjectCode),'design','visual','scr','keys','setting');
