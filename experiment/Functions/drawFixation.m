function drawFixation(col, loc, siz, windowptr)
%
% 2016 by Martin Rolfs
% 2019 by Jan Klanke


loc = loc + [-siz -siz siz siz];
Screen('FillOval', windowptr, col, loc, siz);
end


