function screen2gif(filename)

global scr

frame = Screen('GetImage', scr.myimg);
[imind,cm] = rgb2ind(frame,256);
imwrite(imind,cm,filename,'png');
