function par = getStimulusPars(num)
%
% 2016 by Martin Rolfs
% 2021 by Jan Klanke

global visual scr

if nargin < 1
    num = 1;
end

for n = 1:num
    par.amp(n) = .5;               % amplitude; so 1 means full contrast
    par.frq(n) = 5;                % spatial frequency [cycles/deg] 
    par.ori(n) = 0.0*pi;           % orientation [radians] - 0 and pi are vertical, pi/2 and 3*pi/2 are horizontal
    par.pha(n) = 0.0*pi;           % phase [radians]
    par.tap(n) = 1; % 2/3;         % size of the 'tapered' section 
    par.len(n) = round(scr.resX * scr.dpp * 2/3 );  % length of the stimulus [deg]
    par.hig(n) = round(scr.resY * scr.dpp * 1/10);  % height of the stimulus [deg]
    par.asp(n) = 1;                % aspect ratio of x vs y

    % transform to pixels
    par.frqp(n) = par.frq(n)/visual.ppd;             % spatial frequency [cycles/pix] 
    par.lenp(n) = ceil(par.len(n)*visual.ppd);       % length of the stimulus [pix]
    par.lenp(n) = par.lenp(n) + ~mod(par.lenp(n),2); % length of the stimulus corrected to uneven [pix]
    par.higp(n) = ceil(par.hig(n)*visual.ppd);       % length of the stimulus [pix]
    par.higp(n) = par.higp(n) + ~mod(par.higp(n),2); % length of the stimulus corrected to uneven [pix]
end
