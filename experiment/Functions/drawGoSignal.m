function drawGoSignal(col, loc, siz, windowptr)
%
% 2010 by Martin Rolfs
% 2019 by Jan Klanke


loc = loc + [-siz -siz siz siz];
Screen('FrameOval', windowptr, col, loc, siz/3);
end
