function [vpno, seno, cond, dome, subjectCode, overrideSwitch] = prepExp(experimentName)
%
% 2020 by Jan Klanke
% 
% Input:  experimentName - name of the experiment.
% 
% Output: vpno           - id of the participant
%         seno           - number of the session of the participant
%         cond           - number of the condition/s to be presented
%                          number is supposeed to be overwritten
%         dome           - dominant eye
%         subjectCode    - subject code (experiment name + vpno + seno)
%         overrideSwitch - flag that communicates if default condition
%

FlushEvents('keyDown');
global setting

overrideSwitch = 0;
    
%%%%%%%%%%%%%%%%%%%%%%
% Get participant ID %
%%%%%%%%%%%%%%%%%%%%%%
vpno = [];

while isempty(vpno) 
    % get vpno
    vpno  = input('>>>> Enter participant ID:  ','s');
    
    % make x default for piloting
    if setting.pilot && isempty(vpno); vpno = sprintf('x'); end
    
    % make sure vpno length does not exceed 1 digit
    if length(vpno) > 1 || ~isnan(str2double(vpno))
        fprintf(1, 'WARNING:  Subject code is not a character or too long. Pls only use 1 letter.\n');
        vpno = [];
    end
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get session number (real experiment) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if setting.train == 0
    % collect session no and make string w/ leading 0
    seno = input('>>>> Enter session number:  ','s');
    
    % make "01" default for piloting--warn and repeat if session no is
    % not specified correctly in the main experiment
    if setting.pilot && isempty(seno); seno = sprintf('1'); end
    
    if isempty(seno) || isnan(str2double(seno))
        while isempty(seno) || isnan(str2double(seno))
            fprintf(1, 'WARNING:  Pls sepcify session no. by entering a number.\n');
            seno = input('>>>> Enter session number:  ','s');
        end
    end
    seno = strcat('0',seno);
    
    % create subject code based on experiment name, vopno, and seno.
    subjectCode = sprintf(strcat(experimentName,'_',vpno,seno));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Get conditions (real experiment)   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % define defaults
    if all(seno == '01')
        defaultConds = [1 2 3];
    else
        defaultConds = [1 4 5];
    end
    cond = NaN(1,length(defaultConds));
       
    % default to pre-defined conditions when NOT piloting
    if ~setting.pilot
       cond = defaultConds;
    else
        % ask/repeat asking when piloting
        while numel(intersect(cond, defaultConds)) < numel(cond)
            cond = input('>>>> Enter condition numbers:  ','s');
            % Check whether input is a number.
            if isempty(str2num(cond)) && ~isempty(cond)
                fprintf(1, 'WARNING:  At least one of the conditions you specified does not exist. \nWARNING:  You can normally only choose between conditions: %s\n', num2str(defaultConds));
                continue;
            end
            % Check whether input is set to 'special'.
            if ~isempty(cond); cond = str2num(cond);
            elseif isempty(cond); cond = defaultConds;
            end
            % Check whether conditions (either special or not) are appropriate
            % for session.
            if length(intersect(cond, defaultConds)) < length(cond)
                fprintf(1, 'WARNING:  At least one of the conditions you specified does not exist. \nWARNING:  You can normally only choose between conditions: %s\n',num2str(defaultConds));
                fprintf(1, 'WARNING:  However, I can overwrite the defaults for this session.\n');
                overwrite = input( 'WARNING:  Do you want me to override the session defaults [y / n]? ','s');
                if strcmp(overwrite,'y'); overrideSwitch = 1; break; end
            end
        end
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default all values (except vpno) for training %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif setting.train == 1
    seno = '01'; 
    subjectCode = sprintf(strcat(experimentName,'p',vpno,seno));
    
    resp = '';
    while ~any(strcmp(resp, {'0', '1', '2'}))
        resp = input('>>>> Do you want to train stimulus detection (0) or practice pursuit (1) or real experiment (2):  ','s');
    end
    if resp == '0' && ~setting.TEST
        resp2 = input( 'WARNING:  Eyetracking currently enabled; I will switch this off.\nWARNING:  If you are sure that you want to proceed with tracking, press y (press anything else otherwise).', 's');
        if isempty(resp2) || resp2 ~= 'y'; setting.TEST = 2; end
    end
    resp = str2num(resp);
    cond = 6 + resp;
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make sure scale dir is set up correctly  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
defaultSDir = {'Yes -> No', 'No -> Yes'};
dirr = [];

if isnan(setting.sdir)
    allVpNo = 'abcdeghijklmnopqrsuvwxyz';
    setting.sdir = mod(find(vpno == allVpNo),2);
end

% show pre-defined scale direction
fprintf(1,'>>>> The affixed scale direction is: %s\n', defaultSDir{setting.sdir + 1});
% default to accept when piloting
if setting.pilot
    dirr = input('>>>> Are you sure this is the scale direction you want to use [y / n]?','s');
    if isempty(dirr); dirr = 'y'; end
end
% ask till explicit confirmation is given that pre-defined scale direction
% is correct...
while isempty(dirr) 
    dirr = input('>>>> Are you sure this is the scale direction you want to use [y / n]?','s');
    % ...or change direction if its not
    while dirr ~= 'y' 
        setting.sdir = 1 - setting.sdir;
        fprintf('>>>> So you want the scale direction to be: %s\n', defaultSDir{setting.sdir + 1});
        dirr = input('>>>> Are you sure this is the scale direction you want to use [y / n]?','s');
    end
end
    
%%%%%%%%%%%%%%%%%%%%%%%%
%   Get dominant eye   %
%%%%%%%%%%%%%%%%%%%%%%%%
defaultEye = ['L', 'R', 'B'];
dome = NaN;

% ask for dominant eye 
while length(intersect(dome, defaultEye)) < length(dome) || isempty(dome)
    dome = input('>>>> Enter dominant eye [e.g. L, R, B]:  ','s');
    % repeat question/warn (yadayadayada) when dominant eye is not
    % specified correctly
    if isempty(dome)
        % ...except during piloting b/c it does not matter
        if setting.pilot; dome = 'R'; 
        else  fprintf(1, 'WARNING:  Please specify the dominant eye. \nWARNING:  You can choose between the following options: L, R, B\n'); 
        end
    end
    if length(intersect(dome, defaultEye)) < length(dome); fprintf(1, 'WARNING:  You made a mistake when trying to specify the dominant eye. \nWARNING:  You can choose between the following options: L, R, B\n'); end
end
% hand the dominant eye through to settings parameter
if strcmp(dome,'R')
    setting.DOMEYE = 2;
elseif strcmp(dome,'L')
    setting.DOMEYE = 1;
else
    setting.DOMEYE = 2;
    fprintf(1, 'WARNING:  S/th went wrong with specifying the dominant eye. \nWARNING:  I defaultet to the right eye now. Pls abort/restart the script and enter the data carfully.\n');
end
end
 