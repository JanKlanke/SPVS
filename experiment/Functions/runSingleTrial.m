function [data,eyeData] = runSingleTrial(td)
% 2016 by Martin Rolfs 
% 2021 by Jan Klanke
% 
% Input:  td      - part of the design structure that pertains to the
%                   trial to be displayed
% 
% Output: data    - trial parameters and behavioral results
%         eyeData - flag for eyetracking  

global scr visual setting keys 

% clear keyboard buffer
FlushEvents('KeyDown');
if ~setting.TEST == 1, HideCursor(); end
pixx = setting.Pixx; 

paren = @(x, varargin) x(varargin{:});

% Set the transparency for the stimulus (Gabor patch or sine w/ raised
% cosine masketc.)
% Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

% predefine boundary information
cxm = td.bound.pos;
cym = td.bound.pos;
rad = td.bound.rad;
chk = td.bound.rad;

% draw trial information on operator screen
if ~setting.TEST, Eyelink('command','draw_box %d %d %d %d 15', (cxm(1,  1)-chk(1))*2, (cym(1,  2)-chk(1))*2, (cxm(1,  1)+chk(1))*2, (cym(1,  2)+chk(1))*2); end
if ~setting.TEST, Eyelink('command','draw_box %d %d %d %d 15', (cxm(end,1)-chk(1))*2, (cym(end,2)-chk(1))*2, (cxm(end,1)+chk(1))*2, (cym(end,2)+chk(1))*2); end

% generate donut-shaped stimulus
nStim = length(td.stims.pars.lenp);
if ~setting.movie; sti.tex = visual.procStimsTex; end
sti.src = [zeros(2,nStim); td.stims.pars.lenp; td.stims.pars.higp];
sti.dst = CenterRectOnPoint(sti.src, td.stims.locX', td.stims.locY');
for f = 1:td.totNFr; stiFrames(f).dst = sti.dst + repmat(reshape(td.posVec(:,f),[],nStim),nStim,1); end

% predefine time stamps
tFixaOn = NaN;  % t of fixation stream on
tStimOn = NaN;  % t of stimulus stream on
tStimCf = NaN;  % t of stimulus at full contrast
tSacIni = NaN;  % t of saccade struction initiation
tStimMS = NaN;  % t of stimulus starting to move
tStimMT = NaN;  % t of stimulus at peak velocity
tStimCd = NaN;  % t of stimulus contrast starts to decline
tStimOf = NaN;  % t of stimulus off
tRes    = NaN;  % t of response (if any)
tClr    = NaN;  % t of of clear screen

% set flags before starting stimulus stream
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;
       
% Data
eyeData   = [];

% Initialize vector important for response
pressed = 0;
resTier = 1;   
response= NaN(1,3);

set_movie = setting.movie;
if set_movie
    if td.sacMod; mname = sprintf('%s_%s_saccade_%s_targetVel_%s_dist.mov', cell2mat(paren({'early', 'late'}, td.swiTim+1)), cell2mat(paren({'active', 'replay'}, td.repJIn+1)), cell2mat(paren({'slow', 'medium', 'fast'}, td.movvel/3)), cell2mat(paren({'short', 'medium', 'long'}, td.sacDst*2)));
    else  mname = sprintf('pursuit_%i_dvas.mov', td.movvel)';
    end
    movie = Screen('CreateMovie', scr.myimg, mname);
end

% Initialize vector to store data on timing
frameTimes = repmat(12*scr.fd,1,td.totNFr);

% flip screen to start out time counter for stimulus frames
firstFlip = 0;nextFlip = 0;
while firstFlip == 0
    if pixx, firstFlip = PsychProPixx('QueueImage', scr.myimg);
    else     firstFlip = Screen('Flip', scr.myimg); end
end

% set frame count to 0
f = 0;              

% get a first timestap
tLoopBeg = GetSecs; 
t = tLoopBeg;

while ~breakIt && f < td.totNFr
    f = f+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % stimulus presentation %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
    for slo = 1:setting.sloMo
        Screen('FillRect', scr.myimg, visual.bgColor);
        % target
        if td.targ.vis(f)
           drawTargets(td.targ.col(:,:,f), td.targ.locX(f,:), td.targ.locY(f,:), td.targ.siz, scr.myimg)     
           % Screen('FrameOval', scr.myimg, scr.black, [-rad(f) -rad(f) rad(f) rad(f)] + [cxm(f,1) cym(f,2) cxm(f,1) cym(f,2)], [], [], []);
        end
        % fixation
        if td.fixa.vis(f)
           drawFixation(td.fixa.col, td.fixa.loc, td.fixa.sin, scr.myimg) 
        end
        % stimulus
        % Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);
        if td.stims.vis(f)
            Screen('DrawTextures', scr.myimg, repmat(sti.tex,nStim,1), sti.src, stiFrames(f).dst, td.stims.pars.ori', [], td.stims.amp(:,f)', [], [], [], [zeros(1,nStim); td.stims.pha(:,f)'; zeros(1,nStim); zeros(1,nStim)]);
        end
        % flip
        if set_movie; Screen('AddFrameToMovie', scr.myimg);end
        if pixx; nextFlip = PsychProPixx('QueueImage', scr.myimg);
        else     nextFlip = Screen('Flip', scr.myimg); end
    end
    frameTimes(f) = frameTimes(f) + GetSecs;   
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    % raise stimulus flags %
    %%%%%%%%%%%%%%%%%%%%%%%%
    % Send message that stimulus is now on
    if isnan(tFixaOn) && td.events(f)==1
        if ~setting.TEST  ; Eyelink('message', 'EVENT_FixaOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_FixaOn'); end
        tFixaOn = frameTimes(f);
    end
    if isnan(tStimOn) && td.events(f)==2
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOn'); end
        tStimOn = frameTimes(f);
    end
    if isnan(tStimCf) && td.events(f)==3
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCf'); end
        tStimCf = frameTimes(f);
    end
    if isnan(tSacIni) && td.events(f)==4
        if ~setting.TEST  ; Eyelink('message', 'EVENT_SacIni'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_SacIni'); end
        tSacIni = frameTimes(f);
    end
    if isnan(tStimMS) && td.events(f)==5
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMS'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMS'); end
        tStimMS = frameTimes(f);
    end
    if isnan(tStimMT) && td.events(f)==6
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMT'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMT'); end
        tStimMT = frameTimes(f);
    end
    if isnan(tStimCd) && td.events(f)==7
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCd'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCd'); end
        tStimCd = frameTimes(f);
    end
    if isnan(tStimOf) && td.events(f)==8
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOf'); end
        tStimOf = frameTimes(f);
    end
    
    % eye position check
    if setting.TEST<2
        [xl,xr,yl,yr] = getCoords;
        if sqrt((xr-cxm(f,1))^2+(yr-cym(f,2))^2)>chk(f) || sqrt((xl-cxm(f,1))^2+(yl-cym(f,2))^2)>chk(f)   % check fixation in a circular area
            fixBreak = 1;
        end
    end
    if fixBreak
        breakIt = 1;    % fixation break
    end
end

lastFlip = 0;
while lastFlip == 0 && nextFlip == 0
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);
    else     lastFlip = Screen('Flip', scr.myimg); end 
end
tLoopEnd = GetSecs;

if set_movie; Screen('FinalizeMovie', movie);end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% stimulus presentation 'Blank' %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
for i = 1:scr.rate
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, PsychProPixx('QueueImage', scr.myimg);
    else     Screen('Flip', scr.myimg); end
end
WaitSecs(td.aftKey/4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Break trial or initiate reponse query %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nextRespFlip = 0; 
switch breakIt
    case 1
        % data = 'fixBreak';
        data = 'fixBreak';
        if setting.TEST<2, Eyelink('command','draw_text 100 100 15 Fixation break'); end
    otherwise
        
        % Set init parameters
        tResBeg = GetSecs;delta=0;ticks = td.sOnPos;
        
        % response phase
        done = 0;
        pressed = 0;
        while ~done   
            
            % evaluate response to first 2 questions
            if resTier == 1 || resTier == 3
                if ( td.scale.Dir && checkTarPress(keys.resRghtA)) || (~td.scale.Dir && checkTarPress(keys.resLeftA)) 
                    response(resTier) = 1; 
                    resTier = resTier + 1; 
                    pressed = 1;
                end
                if (~td.scale.Dir && checkTarPress(keys.resRghtA)) || ( td.scale.Dir && checkTarPress(keys.resLeftA))
                    response(resTier) = 0; 
                    resTier = resTier + 1; 
                    pressed = 1;
                end
                % skip resp 2 of resp 1 was 0
                if resTier == 2 && response(1) == 0; resTier = resTier + 1;end
            end
            if resTier == 2
                if checkTarPress(keys.resUpArr)
                    response(resTier) = 1; 
                    resTier = resTier + 1; 
                    pressed = 1;
                 elseif checkTarPress(keys.resDwArr)
                    response(resTier) =-1; 
                    resTier = resTier + 1; 
                    pressed = 1;
                end
            end
            
            % create response graphics for scale
            for i = 1:scr.rate
                Screen('FillRect', scr.myimg, visual.bgColor);         % draw background
                if resTier <= 3; drawScale(td.scale, resTier); end
                if pixx, nextRespFlip = PsychProPixx('QueueImage', scr.myimg);
                else     nextRespFlip = Screen('Flip', scr.myimg); end
            end
            
            % key-press?
            if pressed; WaitSecs(td.aftKey); pressed = 0; end
            
            % end?
            if ~isnan(response(3)); done = 1; end
        end

        % check for final flip
        lastFlip  = 0;
        while ~lastFlip
            if pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);;
            else     lastFlip = Screen('Flip', scr.myimg); end
        end    
        
        % get timing when participants are done
        tRes = GetSecs();
                
        % make one flip with empty screen
        for i = 1:scr.rate
            Screen('FillRect', scr.myimg, visual.bgColor);
            if pixx, PsychProPixx('QueueImage', scr.myimg);
            else     Screen('Flip', scr.myimg); end
        end
        
        % determine response duration 
        tResDur = (tRes - tLoopBeg) * 1000; 
                             
        WaitSecs(td.aftKey);       
        tClr = GetSecs;
        if setting.TEST<2, Eyelink('message', 'EVENT_Clr'); end
        if setting.TEST; fprintf(1,'\nEVENT_Clr'); end 

               
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
        condData = sprintf('%i\t%i',...
            [td.expcon setting.train]);                                     % in results, cells  6:8
        
        % collect stimulus information                                        in results, cells 9:20
        stimData = sprintf('%.2f\t%.2f\t%i\t%i\t%i\t%i\t%.2f\t%i\t%i\t%i\t%.2f\t%i\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%i\t%.2f\t%.2f',...  
            [td.fixpox td.fixpoy td.movdir td.movvel td.stioriUP td.stioriDW td.phavel td.repJIn td.numDot td.sacMod td.sacDst td.sacAng td.appbeg td.apptop td.appdur td.appmdX td.appmdY td.appori td.appvpk td.appamp]);        
        % collect data on the artificial saccade that shaped the stim.        in results, cells 21:26
        asacData = sprintf('%i\t%i\t%.2f\t%.2f\t%.2f\t%.2f',...
            [td.sac.idx td.sac.req td.sac.amp td.sac.vpk td.sac.dur td.sac.ang]);
        
        % collect scale information                                           in results, cells 27:31
        scaleData = sprintf('%i\t%i',...
            [td.scale.Dir td.sOnPos]); 
                           
        % collect time information                                            in results, cells 32:39
        timeData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i',...                             
            round(1000*([tFixaOn tStimOn tStimCf tSacIni tStimMS tStimMT tStimCd tStimOf tRes tClr]-tStimOn)));
        
        % collect response information                                        in results, cells 40:43
        respData = sprintf('%i\t%i\t%i' , ...
            response(1), response(2), response(3)); 

        % get information about how timing of frames worke                    in results, cells 44:45
        tLoopFrames = round((tLoopEnd-tLoopBeg)/scr.fd);        
        frameData = sprintf('%i\t%i',tLoopFrames,td.totNFr);

        % collect data for tab [3 x condData, 10 x trialData, 5 x sacData, 10 x timeData %i, 8 x respData, 2 x frameData]
        data = sprintf('%s\t%s\t%s\t%s\t%s\t%s\t%s',...
            condData, stimData, asacData, scaleData, timeData, respData, frameData);
end
end